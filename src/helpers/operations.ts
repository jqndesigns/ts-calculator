import { maxDec, maxNum } from '../API/calculatorAPI';

export const operate = (
  first: number,
  second: number,
  operation: string,
): number => {
  let result: number = 0;
  switch (operation) {
    case '/':
      result = first / second;
      break;
    case 'x':
      result = first * second;
      break;
    case '-':
      result = first - second;
      break;
    case '+':
      result = first + second;
      break;
    default:
      result = first + second;
      break;
  }
  return Math.min(+result.toFixed(maxDec), maxNum);
};

export const addStringNumber = (current: string, newNumber: string): string => {
  if (current.includes('.') && newNumber === '.') {
    return current;
  }
  if (current === '0' && newNumber === '0') {
    return current;
  }
  if (current === '' && newNumber === '.') {
    return '0.';
  }

  const sum: string = `${current}${newNumber}`;
  let beforeDec: string = sum.split('.')[0];
  beforeDec = beforeDec.length > 1 ? beforeDec.replace(/^0+/, '') : beforeDec;
  beforeDec =
    beforeDec.length > maxNum.toString().length ? maxNum.toString() : beforeDec;

  let afterDec: string = sum.split('.')[1];
  afterDec = afterDec !== undefined ? `.${afterDec}` : '';
  afterDec =
    afterDec.length > maxNum.toString().length + 1
      ? `.${maxNum.toString()}`
      : afterDec;

  return `${beforeDec}${afterDec}`;
};

export const substractStringNumber = (current: string): string => {
  if (
    current === '0' ||
    current === undefined ||
    current.toString().length <= 0 ||
    (current.toString().charAt(0) === '-' && current.toString().length <= 2)
  ) {
    return '';
  }

  return current.slice(0, -1);
};
