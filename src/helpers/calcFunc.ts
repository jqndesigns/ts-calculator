import { operate } from './operations';

export const newCalculation = (
  firstOp: number,
  secondOp: number,
  operation: string,
  result: number,
): number | string => {
  const first: number = firstOp !== undefined ? firstOp : result || 0;
  const second: number = secondOp !== undefined ? secondOp : 0;

  if (operation === '/' && second === 0) {
    return 'No sé dividir entre 0 :(';
  }
  return operate(first, second, operation);
};

export const getOperationFormula = (
  firstOp: string,
  secondOp: string,
  operation: string,
) => `${firstOp.toLocaleString()} ${operation} ${secondOp.toLocaleString()}`;

export const showResult = (result: number, text: string): string => {
  if (text.length) {
    return text;
  }
  return result.toLocaleString();
};
