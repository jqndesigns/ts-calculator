import React, { useEffect, useState } from 'react';

import { allCalculators, allNumbers } from '../../API/calculatorAPI';
import {
  getOperationFormula,
  newCalculation,
  showResult,
} from '../../helpers/calcFunc';
import {
  addStringNumber,
  substractStringNumber,
} from '../../helpers/operations';
import Button from './Button';

const Calculator = () => {
  const [result, setResult] = useState<number>(0);
  const [textResult, setTextResult] = useState<string>('');
  const [firstOp, setFirstOp] = useState<string>('0');
  const [secondOp, setSecondOp] = useState<string>('');
  const [operation, setOperation] = useState<string>('');
  const [newCalc, setNewCalc] = useState<boolean>(true);

  const resetOperation = () => {
    setFirstOp(result.toString());
  };

  const resetCalculator = (resetResult: boolean = true) => {
    setFirstOp('0');
    setSecondOp('');
    setOperation('');
    if (resetResult) {
      setResult(0);
      setTextResult('');
    }
  };

  const calculate = () => {
    const calc: number | string = newCalculation(
      +firstOp,
      +secondOp,
      operation,
      result,
    );
    if (!Number.isNaN(+calc)) {
      setResult(+calc);
      setTextResult('');
    } else {
      setTextResult(calc.toString());
    }
    setNewCalc(true);
  };

  const substractNumberToOperation = () => {
    if (operation === '') {
      setFirstOp((n) => substractStringNumber(n));
    } else if (secondOp !== undefined && secondOp !== '') {
      setSecondOp((n) => substractStringNumber(n));
    } else {
      setSecondOp('');
      setOperation('');
    }
    setNewCalc(false);
  };

  const addNumberToOperation = (value: string) => {
    if (operation === '') {
      setFirstOp((n) => addStringNumber(newCalc ? '' : n, value));
    } else {
      setSecondOp((n) => addStringNumber(newCalc ? '' : n, value));
    }
    setNewCalc(false);
  };

  const addOperatorToOperation = (operator: string) => {
    if (operator === 'C') {
      resetCalculator();
      return;
    }

    if (operator === '←') {
      if (newCalc) {
        resetCalculator(false);
      } else {
        substractNumberToOperation();
      }
      return;
    }

    if (operator === '.') {
      addNumberToOperation('.');
      return;
    }

    if (operation === '' && operator !== '=') {
      setOperation(operator);
    } else if (operator === '=') {
      calculate();
    } else if (secondOp !== undefined && !newCalc) {
      calculate();
      setOperation(operator);
      setSecondOp('');
    } else {
      setOperation(operator);
      setSecondOp('');
    }
  };

  const handleInput = (value: string) => {
    if (!Number.isNaN(+value)) {
      addNumberToOperation(value);
    } else {
      addOperatorToOperation(value);
    }
  };

  useEffect(() => {
    resetOperation();

    if (textResult !== '') {
      setTimeout(() => {
        setTextResult('');
      }, 2000);
    }
  }, [result, textResult]);

  return (
    <div id="calculator">
      <div id="result">{showResult(result, textResult)}</div>
      <div id="operationBox">
        {getOperationFormula(firstOp, secondOp, operation)}
      </div>
      <div id="pad">
        <div id="numbers">
          {allNumbers
            .sort((a, b) => b.order - a.order)
            .map((n) => (
              <Button
                key={n.stringRepresentation}
                keyTriggers={n.keyTrigger}
                size={n.size}
                value={n.stringRepresentation}
                onClickHandler={handleInput}
              />
            ))}
        </div>
        <div id="calculators">
          {allCalculators.map((n) => (
            <Button
              key={n.stringRepresentation}
              keyTriggers={n.keyTrigger}
              size={n.size}
              value={n.stringRepresentation}
              onClickHandler={handleInput}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Calculator;
