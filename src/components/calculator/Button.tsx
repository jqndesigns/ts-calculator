import React from 'react';
import classNames from 'classnames';

import { useKeyDown } from '../../customHooks/useKeyDown';

interface buttonProps {
  keyTriggers: string[];
  onClickHandler: (num: string) => void;
  size: number;
  value: string;
}

const Button = ({ keyTriggers, onClickHandler, size, value }: buttonProps) => {
  const classes = classNames({
    button: true,
    sizeLarge: size === 3,
    sizeMedium: size === 2,
    sizeMini: size === 0,
    sizeSmall: size === 1,
  });

  useKeyDown(() => {
    onClickHandler(value);
  }, [...keyTriggers]);

  return (
    <button
      className={classes}
      type="button"
      onClick={() => onClickHandler(value)}
    >
      <div>{value}</div>
    </button>
  );
};

export default Button;
