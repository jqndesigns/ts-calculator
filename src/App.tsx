import React from 'react';

import Calculator from './components/calculator/Calculator';

function App() {
  return (
    <div id="container">
      <Calculator />
    </div>
  );
}

export default App;
