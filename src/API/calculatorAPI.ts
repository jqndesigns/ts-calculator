import { ButtonType } from '../models/ButtonType';

export const maxNum: number = 999999999999999;

export const maxDec: number = 3;

export const allNumbers: ButtonType[] = [
  {
    keyTrigger: ['.'],
    order: -3,
    size: 1,
    stringRepresentation: '.',
  },
  {
    keyTrigger: ['0'],
    order: -2,
    size: 1,
    stringRepresentation: '0',
  },
  {
    keyTrigger: ['Escape', 'Delete', 'c'],
    order: -1,
    size: 0,
    stringRepresentation: 'C',
  },
  {
    keyTrigger: ['Enter'],
    order: 0,
    size: 0,
    stringRepresentation: '=',
  },
  {
    keyTrigger: ['1'],
    order: 1,
    size: 1,
    stringRepresentation: '1',
  },
  {
    keyTrigger: ['2'],
    order: 2,
    size: 1,
    stringRepresentation: '2',
  },
  {
    keyTrigger: ['3'],
    order: 3,
    size: 1,
    stringRepresentation: '3',
  },
  {
    keyTrigger: ['4'],
    order: 4,
    size: 1,
    stringRepresentation: '4',
  },
  {
    keyTrigger: ['5'],
    order: 5,
    size: 1,
    stringRepresentation: '5',
  },
  {
    keyTrigger: ['6'],
    order: 6,
    size: 1,
    stringRepresentation: '6',
  },
  {
    keyTrigger: ['7'],
    order: 7,
    size: 1,
    stringRepresentation: '7',
  },
  {
    keyTrigger: ['8'],
    order: 8,
    size: 1,
    stringRepresentation: '8',
  },
  {
    keyTrigger: ['9'],
    order: 9,
    size: 1,
    stringRepresentation: '9',
  },
];
export const allCalculators: ButtonType[] = [
  {
    keyTrigger: ['/'],
    order: 0,
    size: 0,
    stringRepresentation: '/',
  },
  {
    keyTrigger: ['*', 'x'],
    order: 0,
    size: 0,
    stringRepresentation: 'x',
  },
  {
    keyTrigger: ['-'],
    order: 0,
    size: 0,
    stringRepresentation: '-',
  },
  {
    keyTrigger: ['+'],
    order: 0,
    size: 0,
    stringRepresentation: '+',
  },
  {
    keyTrigger: ['Backspace'],
    order: 0,
    size: 1,
    stringRepresentation: '←',
  },
];
