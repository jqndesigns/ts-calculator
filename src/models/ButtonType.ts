export interface ButtonType {
    keyTrigger: string[];
    order: number;
    size: number;
    stringRepresentation: string;
}