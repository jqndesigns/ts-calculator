import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Navigate, Routes } from 'react-router-dom';

import App from '../App';
import { store } from '../app/store';

export const AppRouter = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route element={<App />} path="/*">
          <Route element={<Navigate to="/" />} path="*" />
        </Route>
      </Routes>
    </BrowserRouter>
  </Provider>
);
