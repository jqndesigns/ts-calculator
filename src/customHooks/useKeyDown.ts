import { useEffect } from 'react';

export const useKeyDown = (callback: (T?: any) => void, keys: string[]) => {
  const onKeyDown = (event: KeyboardEvent) => {
    if (keys.includes(event.key)) {
      event.preventDefault();
      callback();
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', onKeyDown);

    return () => {
      document.removeEventListener('keydown', onKeyDown);
    };
  }, [onKeyDown]);
};
